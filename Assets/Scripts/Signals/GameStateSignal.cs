public class GameStateSignal
{
    public readonly GameState GameState;

    public GameStateSignal(GameState gameState)
    {
        GameState = gameState;
    }
}