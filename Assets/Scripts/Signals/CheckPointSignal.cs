public class CheckPointSignal
{
    public readonly int ReachedCheckpoint;

    public CheckPointSignal(int reachedCheckpoint)
    {
        ReachedCheckpoint = reachedCheckpoint;
    }
}