using UnityEngine;

public class EnvironmentButton : MonoBehaviour
{
    [SerializeField] private ButtonType _buttonType;
    [SerializeField] private Animator _joinedObject;
    [SerializeField] private CapsuleCollider _capsuleCollider;
    [SerializeField] private Animator _animator;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            _animator.enabled = true;
            switch (_buttonType)
            {
                case ButtonType.Door:
                    _joinedObject.enabled = true;
                    break;
                case ButtonType.Pikes:
                    _joinedObject.enabled = true;
                    break;
                case ButtonType.Window:
                    _joinedObject.enabled = true;
                    _joinedObject.GetComponent<BoxCollider>().isTrigger = true;
                    break;
            }
            _capsuleCollider.enabled = false;
        }
    }
}