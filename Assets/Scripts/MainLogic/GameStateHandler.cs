using Zenject;

public class GameStateHandler
{
    private GameState _currentGameState;

    [Inject] private SignalBus _signalBus;

    [Inject]
    public GameStateHandler()
    {
        _currentGameState = GameState.Start;
        _signalBus.Subscribe<GameStateSignal>(ChangeGameState);
    }

    private void ChangeGameState(GameStateSignal gameStateSignal)
    {
        _currentGameState = gameStateSignal.GameState;
        _signalBus.Fire(new GameStateSignal(_currentGameState));
    }
}