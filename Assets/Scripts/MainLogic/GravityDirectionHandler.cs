using UnityEngine;

public class GravityDirectionHandler
{
    private float _gravityPowerMultiplier;

    public GravityDirectionHandler(GlobalConfig globalConfig, IInputHandler inputHandler)
    {
        _gravityPowerMultiplier = globalConfig.GravityMultiplier;
    }
    
    public void ChangeGravity(Vector2 direction)
    {
        Physics.gravity = direction * _gravityPowerMultiplier;
    }
}

