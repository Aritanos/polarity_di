using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Start,
    Finish,
    Win,
    Fail,
    Moving,
    Standing
}
