public enum SwipeDirection
{
    None,
    Left,
    Right,
    Up,
    Down
}