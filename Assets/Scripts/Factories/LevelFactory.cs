using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

public class LevelFactory : IFactory<string, UniTask<LevelObject>>
{
    private DiContainer _container;
    private LevelObject _levelObject;

    public LevelFactory(DiContainer container)
    {
        _container = container;
    }

    public async UniTask<LevelObject> Create(string label)
    {
        _levelObject = await CreateLevel(label);
        return _levelObject;
    }

    private async UniTask<LevelObject> CreateLevel(string label)
    {
        var levelAsset = await AddressablesLoader.LoadAssetAsync<GameObject>(label);
        if (levelAsset == null)
            return null;
        _levelObject = _container.InstantiatePrefabForComponent<LevelObject>(levelAsset);
        return _levelObject;
    }
}