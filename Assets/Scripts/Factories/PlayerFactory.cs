using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

public class PlayerFactory : IFactory<string, UniTask<Player>>
{
    private DiContainer _container;
    private PlayerMovement _playerMovement;
    
    [Inject]
    public void InjectPlayerMovement(PlayerMovement playerMovement)
    {
        _playerMovement = playerMovement;
    }
    
    public PlayerFactory(DiContainer container)
    {
        _container = container;
    }

    public async UniTask<Player> Create(string label)
    {
        var levelAsset = await AddressablesLoader.LoadAssetAsync<GameObject>(label);
        var instantiatedPlayerAsset = _container.InstantiatePrefabForComponent<Player>(levelAsset);
        _playerMovement.SetPlayer(instantiatedPlayerAsset);
        return instantiatedPlayerAsset;
    }
}