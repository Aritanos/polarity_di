using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;

public static class AddressablesLoader
{
    public static async UniTask<IList<T>> LoadAssetsAsync<T>(params string[] labels)
    {
        return await Addressables.LoadAssetsAsync<T>(labels.ToList(), null);
    }

    public static async UniTask<T> LoadAssetAsync<T>( params string[] labels)
    {
        var location = await Addressables.LoadResourceLocationsAsync(labels[0]);
        if (location.Count == 0)
        {
            return default;
        }
        if (labels.Length == 1)
            return await Addressables.LoadAssetAsync<T>(labels[0]);
        return await Addressables.LoadAssetAsync<T>(labels.ToList());
    }
}