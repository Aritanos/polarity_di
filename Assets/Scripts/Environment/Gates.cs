using UnityEngine;
using Zenject;

public class Gates : MonoBehaviour
{
    [SerializeField] private HingeJoint _leftHingeJoint;
    [SerializeField] private HingeJoint _rightHingeJoint;
    [SerializeField] private Rigidbody _rightRB;
    [SerializeField] private Rigidbody _leftRB;
    [SerializeField] private Checkpoint _checkpoint;

    private int _pointNumber;
    
    [Inject]
    private void Initialize(SignalBus signalBus)
    {
        signalBus.Subscribe<CheckPointSignal>(ActivateGates);
        _rightRB.isKinematic = true;
        _leftRB.isKinematic = true;
        _pointNumber = _checkpoint.PointNumber;
    }
    
    private void SetLeftLimits()
    {
        JointLimits limits = _leftHingeJoint.limits;
        limits.max = -75;
        _leftHingeJoint.limits = limits;
    }

    private void SetRightLimits()
    {
        JointLimits limits = _rightHingeJoint.limits;
        limits.min = 75;
        _rightHingeJoint.limits = limits;
    }
    
    private void ActivateGates(CheckPointSignal checkPointSignal)
    {
        if (checkPointSignal.ReachedCheckpoint == _pointNumber)
        {
            _rightRB.isKinematic = false;
            _leftRB.isKinematic = false;
        }
    }

    private void Update()
    {
        if (_leftHingeJoint.angle<-75)
        {
            SetLeftLimits();
        }
        if (_rightHingeJoint.angle > 75)
        {
            SetRightLimits();
        }
    }
}