using UnityEngine;
using Zenject;

public class Trap : MonoBehaviour
{
    [Inject] private SignalBus _signalBus;
    
    [SerializeField] private Checkpoint _checkpoint;
    [SerializeField] private ParticleSystem _nova;

    private bool _isActive;
    private PlayerHealth _playerHealth;
    
    [Inject]
    private void Initialize(PlayerHealth playerHealth)
    {
        _playerHealth = playerHealth;
        _signalBus.Subscribe<CheckPointSignal>(OnCheckpointReached);
    }
    
    private void OnCheckpointReached(CheckPointSignal checkPointSignal)
    {
        if (_checkpoint.PointNumber == checkPointSignal.ReachedCheckpoint)
        {
            _isActive = true;
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if ((other.CompareTag("Weapon")|| other.CompareTag("Enemy"))&& _isActive)
        {
            _nova.Play();
            _playerHealth.ZeroHealth();
            Destroy(this);
        }
    }
}
