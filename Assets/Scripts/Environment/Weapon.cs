using UnityEngine;
using Zenject;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Checkpoint _checkpoint;
    [SerializeField] private Rigidbody _rigidBody;

    [Inject]
    private void Initialize()
    {
        _rigidBody.isKinematic = true;
        _checkpoint.OnCheckpointActivated += EnableWeaponPhysics;
    }

    private void EnableWeaponPhysics(int checkPointNumber)
    {
        if (_checkpoint.PointNumber == checkPointNumber)
        {
            _rigidBody.isKinematic = false;
        }
    }
}
