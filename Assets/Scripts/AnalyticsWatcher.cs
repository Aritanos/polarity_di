using Zenject;

public class AnalyticsWatcher
{
    private IntegrationManager _integrationManager;
    
    [Inject]
    public AnalyticsWatcher(IntegrationManager integrationManager) => _integrationManager = integrationManager;

    public void OnLevelStart(int levelNumber) => _integrationManager.OnLevelStart(levelNumber);

    public void OnGameFailed(int levelNumber) => _integrationManager.OnLevelFail(levelNumber);

    public void OnGameWon(int levelNumber) => _integrationManager.OnLevelFinish(levelNumber);
}