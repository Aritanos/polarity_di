using System;
using UnityEngine;
using Zenject;

public class MouseInputHandler : ITickable, IInputHandler
{
    private Vector2 _startingMousePosition = new (10000, 10000);
    private float _sensitivity;
    
    public event Action<SwipeDirection> OnSwipe;
    
    public MouseInputHandler(GlobalConfig globalConfig)
    {
        _sensitivity = globalConfig.MouseSensitivity;
    }
    
    public void Tick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startingMousePosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            if (_startingMousePosition == new Vector2(10000, 10000))
                return;
            var mousePosition = Input.mousePosition;
            var angle = Vector2.SignedAngle(Vector2.right, (Vector2) Input.mousePosition - _startingMousePosition);
            var distance = Vector2.Distance(_startingMousePosition, mousePosition);
            if (distance < _sensitivity)
                return;
            switch (angle)
            {
                case < 45 and > -45:
                    OnSwipe?.Invoke(GetDirectionFromVector(Vector2.right));
                    break;
                case > 45 and < 135:
                    OnSwipe?.Invoke(GetDirectionFromVector(Vector2.up));
                    break;
                case >135 and < 180 or >-180 and < -135:
                    OnSwipe?.Invoke(GetDirectionFromVector(Vector2.left));
                    break;
                case >-135 and < -45:
                    OnSwipe?.Invoke(GetDirectionFromVector(Vector2.down));
                    break;
            }
            _startingMousePosition = new (10000, 10000);;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _startingMousePosition = new (10000, 10000);;
        }
    }

    private SwipeDirection GetDirectionFromVector(Vector2 vector)
    {
        if (vector == Vector2.down)
            return SwipeDirection.Down;
        if (vector == Vector2.right)
            return SwipeDirection.Right;
        if (vector == Vector2.up)
            return SwipeDirection.Up;
        if (vector == Vector2.left)
            return SwipeDirection.Left;
        return SwipeDirection.None;
    }
}