using System;
using UnityEngine;

public interface IInputHandler
{
    public event Action<SwipeDirection> OnSwipe;
}