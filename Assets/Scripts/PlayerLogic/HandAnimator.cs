using UnityEngine;
using Zenject;

public class HandAnimator : MonoBehaviour
{
    [SerializeField] private Animator _handAnimator;

    private GravityDirectionHandler _gravityDirectionHandler;
    private IInputHandler _inputHandler;

    public void OnSwipe(SwipeDirection swipeDirection)
    {
        Debug.Log("Get swipe");
        switch (swipeDirection)
        {
            case SwipeDirection.Down:
                _handAnimator.SetTrigger("Down");
                break;
            case SwipeDirection.Left:
                _handAnimator.SetTrigger("Left");
                break;
            case SwipeDirection.Up:
                _handAnimator.SetTrigger("Up");
                break;
            case SwipeDirection.Right:
                _handAnimator.SetTrigger("Right");
                break;
        }
    }

    public void SetGravityDown()
    {
        _gravityDirectionHandler.ChangeGravity(Vector2.down);
    }

    public void SetGravityUp()
    {
        _gravityDirectionHandler.ChangeGravity(Vector2.up);
    }

    public void SetGravityLeft()
    {
        _gravityDirectionHandler.ChangeGravity(Vector2.left);
    }

    public void SetGravityRight()
    {
        _gravityDirectionHandler.ChangeGravity(Vector2.right);
    }
    
    [Inject]
    private void Initialize(IInputHandler inputHandler, GravityDirectionHandler gravityDirectionHandler)
    {
        _inputHandler = inputHandler;
        _gravityDirectionHandler = gravityDirectionHandler;
        _inputHandler.OnSwipe+=OnSwipe;
    }

    private void OnDestroy()
    {
        _inputHandler.OnSwipe-=OnSwipe;
    }
}