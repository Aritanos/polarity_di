using UnityEngine;
using Zenject;

public class PlayerMovement: ITickable
{
    private bool _isMoving;
    private float _speed;
    private Transform _playerTransform;

    [Inject] private SignalBus _signalBus;
    
    [Inject]
    public PlayerMovement(PlayerConfig playerConfig)
    {
        _speed = playerConfig.PlayerSpeed;
    }

    public void SetPlayer(Player player)
    {
        _playerTransform = player.transform;
    }

    [Inject]
    public void InjectSignal(SignalBus signalBus)
    {
        _signalBus.Subscribe<GameStateSignal>(SetMovement);
    }

    public void SetMovement(GameStateSignal gameStateSignal)
    {
        _isMoving = gameStateSignal.GameState == GameState.Moving;
    }

    public void Tick()
    {
        if (_playerTransform != null && _isMoving)
            _playerTransform.Translate(Vector3.forward * _speed * Time.deltaTime);
    }
}