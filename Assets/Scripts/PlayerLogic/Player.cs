using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

public class Player : MonoBehaviour
{
    private float _currentHealth;
    
    [Inject] private SignalBus _signalBus;
    
    public class Factory : PlaceholderFactory<string, UniTask<Player>>
    {
        
    }

    public void SetStartingPosition(Transform position)
    {
        transform.position = position.position;
        _signalBus.Fire(new GameStateSignal(GameState.Moving));
    }

    public void DestroySelf()
    {
        Destroy(gameObject);    
    }
    
    private void Start()
    {
        _signalBus.Subscribe<GameStateSignal>(OnGameStateChanged);
    }

    private void OnGameStateChanged(GameStateSignal gameStateSignal)
    {
        //if (gameStateSignal.GameState is GameState.Finish or GameState.Fail) ;
        //HandAnimator.SetTrigger("End");
    }
}