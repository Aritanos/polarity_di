using System;
using Zenject;

public class PlayerHealth
{
    public event Action<int> OnHealthChanged;
    
    private int _health;
    private PlayerConfig _playerConfig;
    private SignalBus _signalBus;
    
    [Inject]
    public PlayerHealth(PlayerConfig playerConfig, SignalBus signalBus)
    {
        _playerConfig = playerConfig;
        _health = _playerConfig.StartingHP;
        _signalBus = signalBus;
        _signalBus.Subscribe<GameStateSignal>(ResetHealth);
    }

    public void ZeroHealth()
    {
        ChangeHealth(-_health);
    }
    
    public void ChangeHealth(int healthChange)
    {
        _health += healthChange;
        OnHealthChanged?.Invoke(_health);
        if (_health <= 0)
        {
            _signalBus.Fire(new GameStateSignal(GameState.Fail));
        }
    }
    
    private void ResetHealth(GameStateSignal gameStateSignal)
    {
        if (gameStateSignal.GameState != GameState.Start) 
            return;
        _health = _playerConfig.StartingHP;
        OnHealthChanged?.Invoke(_health);
    }
}