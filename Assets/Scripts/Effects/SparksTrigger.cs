using UnityEngine;

public class SparksTrigger : MonoBehaviour
{
    [SerializeField] private ParticleSystem _sparks;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Walls"))
        {
            _sparks.Play();
        }    
    }
}
