using UnityEngine;
using Zenject;

public class SwingVFXHandler
{
    private SignalBus _signalBus;
    private ParticleSystem _swingParticleSystem;
    
    public SwingVFXHandler(SignalBus signalBus, ParticleSystem swingParticleSystem)
    {
        _signalBus = signalBus;
        _swingParticleSystem = swingParticleSystem;
        
    }

    private void Swing(Vector2 direction)
    {
        if (direction == Vector2.left)
        {
            _swingParticleSystem.transform.eulerAngles = new Vector3(-90, 225, 0);//Quaternion.Euler(-90, 45, 0);//new Quaternion(-90, 45, 0, 0);
        }
        else if (direction == Vector2.right)
        {
            _swingParticleSystem.transform.eulerAngles = new Vector3(90, -45, 0);
            
        }
        else if (direction == Vector2.up)
        {
            _swingParticleSystem.transform.eulerAngles = new Vector3(0, 90, -225);
        }
        else if (direction == Vector2.down)
        {
            _swingParticleSystem.transform.eulerAngles = new Vector3(0, -90, -25);
        }
    }
}