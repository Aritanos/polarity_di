using System.Collections;
using UnityEngine;
using Zenject;

public class FlashPanel : MonoBehaviour
{
    private float _flashingDuration;

    public void Activate(int playerHealth)
    {
        gameObject.SetActive(true);
        StartCoroutine(ShowFlashIEnum());
    }
    
    [Inject]
    private void Initialize(PlayerHealth playerHealth)
    {
        playerHealth.OnHealthChanged += Activate;
    }
    
    private IEnumerator ShowFlashIEnum()
    {

        yield return new WaitForSeconds(_flashingDuration);
        gameObject.SetActive(false);
    }
}