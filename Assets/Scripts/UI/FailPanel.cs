using UnityEngine;
using Zenject;

public class FailPanel : MonoBehaviour
{
    private LevelSpawnHandler _levelSpawnHandler;

    [Inject] private SignalBus _signalBus;
    
    public void RestartLevel()
    {
        _signalBus.Fire(new GameStateSignal(GameState.Start));
        
        _levelSpawnHandler.RespawnCurrentLevel();
    }
    
    [Inject]
    private void Initialize(LevelSpawnHandler levelSpawnHandler)
    {
        _levelSpawnHandler = levelSpawnHandler;
        _signalBus.Subscribe<GameStateSignal>(ShowPanel);
    }
    
    private void ShowPanel(GameStateSignal gameStateSignal)
    {
        if (gameStateSignal.GameState == GameState.Fail)
            gameObject.SetActive(true);
        if (gameStateSignal.GameState  == GameState.Start)
            gameObject.SetActive(false);
    }
}