using TMPro;
using UnityEngine;
using Zenject;

public class PlayerHealthPanel: MonoBehaviour
{
    [SerializeField] private TMP_Text _text;

    public void SetCurrentHealth(int health)
    {
        _text.text = health.ToString();
    }
    
    [Inject]
    private void Initialize(PlayerHealth playerHealth)
    {
        playerHealth.OnHealthChanged += SetCurrentHealth;
    }
}