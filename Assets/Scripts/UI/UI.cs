using UnityEngine;

public class UI : MonoBehaviour
{
    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.E))
        {
            Time.timeScale = 0;
        }

        else if (Input.GetKeyDown(KeyCode.Space))
        {
            Time.timeScale = 1;
        }
#elif UNITY_ANDROID
        if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    _currentSwipe = SwipeDirection.None;
                    _initialPosition = Input.GetTouch(0).position;
                    _swiped = false;
                }
                else
                {
                    GetSwipeDirection(_initialPosition, touch.position);
                }
            }
#endif
    }
}