using UnityEngine;
using Zenject;

public class WinPanel : MonoBehaviour
{
    private LevelSpawnHandler _levelSpawnHandler;

    [Inject] private SignalBus _signalBus;
    
    [Inject]
    private void Initialize(LevelSpawnHandler levelSpawnHandler)
    {
        _levelSpawnHandler = levelSpawnHandler;
        _signalBus.Subscribe<GameStateSignal>(ShowPanel);
    }

    private void ShowPanel(GameStateSignal gameStateSignal)
    {
        if (gameStateSignal.GameState == GameState.Finish)
            gameObject.SetActive(true);
        if (gameStateSignal.GameState  == GameState.Start)
            gameObject.SetActive(false);
    }
    
    public void NextLevel()
    {
        _signalBus.Fire(new GameStateSignal(GameState.Start));
        _levelSpawnHandler.SpawnNextLevel();
    }
    
    public void RestartLevel()
    {
        _signalBus.Fire(new GameStateSignal(GameState.Start));
        _levelSpawnHandler.RespawnCurrentLevel();
    }
}