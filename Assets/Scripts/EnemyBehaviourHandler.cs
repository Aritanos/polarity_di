public abstract class EnemyBehaviour
{
    protected Enemy _enemy;

    public EnemyBehaviour(Enemy enemy) => _enemy = enemy;
}

public class EnemyStandingBehaviour: EnemyBehaviour
{
    
    public EnemyStandingBehaviour(Enemy enemy) : base(enemy)
    {
        enemy.DisableMoving();
        enemy.DisableRagdoll();
        enemy.EnemyVisualsHandler.SetAnimationState(false);
        enemy.EnemyVisualsHandler.StopParticles();
    }
}

public class EnemyAttackingBehaviour : EnemyBehaviour
{
    public EnemyAttackingBehaviour(Enemy enemy) : base(enemy)
    {
        enemy.StartAttacking();  
        enemy.EnemyVisualsHandler.SetAnimationState(true);
        enemy.EnemyVisualsHandler.StartParticles();
    }
}

public class EnemyDeadBehaviour : EnemyBehaviour
{
    public EnemyDeadBehaviour(Enemy enemy) : base(enemy)
    {
        enemy.EnableRagdoll();
        enemy.StopAttacking();
        enemy.EnemyVisualsHandler.SetAnimatorEnabled(false);
        enemy.EnemyVisualsHandler.StopParticles();
    }
}
