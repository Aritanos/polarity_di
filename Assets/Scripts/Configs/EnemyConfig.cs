using UnityEngine;

[CreateAssetMenu(menuName = "Create EnemyConfig", fileName = "EnemyConfig", order = 0)]
public class EnemyConfig : ScriptableObject
{
    public int AttackPeriod => _attackPeriod;
    public int DamageAmount => _damageAmount;
    
    [SerializeField] protected int _attackPeriod;
    [SerializeField] protected int _damageAmount;
}