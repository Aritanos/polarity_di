using UnityEngine;

[CreateAssetMenu(menuName = "Create Global Config", fileName = "Global Config", order = 0)]
public class GlobalConfig : ScriptableObject
{
    public float MouseSensitivity => _mouseSensitivity;
    public float GravityMultiplier => _gravityMultiplier;
    
    [SerializeField] private float _mouseSensitivity;
    [SerializeField] private float _gravityMultiplier;
}
