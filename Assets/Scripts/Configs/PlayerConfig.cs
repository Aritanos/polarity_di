using UnityEngine;

[CreateAssetMenu(menuName = "Create PlayerConfig", fileName = "PlayerConfig", order = 0)]
public class PlayerConfig : ScriptableObject
{
    public float PlayerSpeed => _playerSpeed;
    public int StartingHP => _startingHP;
    
    [SerializeField] private float _playerSpeed;
    [SerializeField] private int _startingHP;
}