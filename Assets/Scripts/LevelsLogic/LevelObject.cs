using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

public class LevelObject : MonoBehaviour
{
    public Transform StartPosition => _startPosition;

    [SerializeField] private Transform _startPosition;

    public class Factory : PlaceholderFactory<string, UniTask<LevelObject>> { }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
