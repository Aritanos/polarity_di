using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Checkpoint : MonoBehaviour
{
    public int PointNumber => _pointNumber;
    
    [SerializeField] private int _pointNumber;
    [SerializeField] private List<Enemy> _enemies;
    [SerializeField] private Animator _doorAnimator;

    public event Action<int> OnCheckpointActivated;
    
    public void OnEnemyDefeated(Enemy enemy)
    {
        _enemies.Remove(enemy);
        if (_enemies.Count == 0) 
        {
            _signalBus.Fire(new GameStateSignal(GameState.Moving));
            if (PointNumber != 2)
            {
                _doorAnimator.SetTrigger("Open");
            }
        }
    }
    
    [Inject]
    private SignalBus _signalBus;

    private void Awake()
    {
        foreach (var enemy in _enemies)
        {
            enemy.SetCheckpoint(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Physics.gravity = Vector3.zero;
            OnCheckpointActivated?.Invoke(_pointNumber);
            _signalBus.Fire(new GameStateSignal(GameState.Standing));
        }
    }

}
