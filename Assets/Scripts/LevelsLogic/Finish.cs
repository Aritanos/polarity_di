using UnityEngine;
using Zenject;

public class Finish : MonoBehaviour
{
    [SerializeField] private LevelEndingMechanism _levelEndingMechanism;

    [Inject] public SignalBus _signalBus;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _signalBus.Fire(new GameStateSignal(GameState.Standing));
            _levelEndingMechanism.Enable();
        }    
    }
}