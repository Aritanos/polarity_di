using UnityEngine;

public class LevelSpawnHandler
{
    public Transform SpawnTransform => _levelObject.transform;
    private LevelObject.Factory _levelFactory;
    private Player.Factory _playerFactory;
    private LevelObject _levelObject;
    private Player _player;
    private int _currentLevelNumber;

    public LevelSpawnHandler(LevelObject.Factory levelLevelFactory, Player.Factory playerFactory)
    {
        _levelFactory = levelLevelFactory;
        _playerFactory = playerFactory;
        
#if UNITY_EDITOR
        _currentLevelNumber = 0;
#elif UNITY_ANDROID
        _currentLevelNumber = PlayerPrefs.GetInt("CurrentLevelNumber", 0);
#endif
        SpawnNewLevel(_currentLevelNumber);
    }

    public void SpawnNewLevel(int levelNumber)
    {
        SpawnLevel(levelNumber);
    }

    public void SpawnNextLevel()
    {
        SpawnLevel(++_currentLevelNumber);
    }

    public void RespawnCurrentLevel()
    {
        SpawnLevel(_currentLevelNumber);
    }

    private async void SpawnLevel(int levelNumber)
    {
        if (_levelObject != null)
            _levelObject.DestroySelf();
        if (_player != null)
            _player.DestroySelf();
        _player = await _playerFactory.Create("Player");
        _levelObject = await _levelFactory.Create("Level" + levelNumber);
        if (_levelObject == null)
        {
            _currentLevelNumber = 0;
            _levelObject = await _levelFactory.Create("Level" + _currentLevelNumber);
            if (_levelObject == null)
            {
                Debug.LogError("Level asset 0 doesn't exist");
                return;
            }
        }
        _player.SetStartingPosition(_levelObject.StartPosition);
    }
}