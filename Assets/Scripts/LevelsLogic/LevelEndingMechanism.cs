using UnityEngine;
using Zenject;

public class LevelEndingMechanism: MonoBehaviour
{
    [SerializeField] private ParticleSystem _sparkle;
    [SerializeField] private Animator _levelEndingMechanism;
    [SerializeField] private Animator _doorsAnimator;
    
    private bool _isActive;
    private IInputHandler _inputHandler;

    [Inject] private SignalBus _signalBus;

    public void Enable()
    {
        Debug.Log("Enable Finish");
        _isActive = true;
    }

    public void EndLevel()
    {
        _signalBus.Fire(new GameStateSignal(GameState.Finish));
    }
    
    [Inject]
    private void Initialize(IInputHandler inputHandler)
    {
        _inputHandler = inputHandler;
        _inputHandler.OnSwipe += Activate;
    }

    private void Activate(SwipeDirection direction)
    {
        if (!_isActive)
            return;
        if (direction == SwipeDirection.Down )
        {
            _levelEndingMechanism.SetTrigger("Activate");
            _doorsAnimator.SetTrigger("Open");
        }
    }

    private void OnDestroy()
    {
        _inputHandler.OnSwipe -= Activate;
    }
}