﻿using System;
using System.Collections;
using System.Collections.Generic;
//using Cinemachine;
using UnityEngine;
//using Facebook.Unity;
using GameAnalyticsSDK;
using com.adjust.sdk;
using Zenject;

public class IntegrationManager : MonoBehaviour
{
    [Inject]
    private void Initialize()
    {
        GameAnalytics.Initialize();
#if UNITY_IOS
        /* Mandatory - set your iOS app token here */
        InitAdjust("wgg3d8yjidxc");
#elif UNITY_ANDROID
        /* Mandatory - set your Android app token here */
        InitAdjust("aggqt94g19ts");
#endif
    }

    public void OnLevelStart(int level)
    {
        #region Facebook
        //LogFacebookEvent("StartLevel", "level", level.ToString());
        #endregion

        #region GameAnalytics
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level_" + level);
        #endregion
    }

    public void OnLevelFinish(int level)
    {
        #region Facebook
        //LogFacebookEvent("CompleteLevel", "level", level.ToString());
        #endregion

        #region GameAnalytics
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level_" + level);
        #endregion
    }

    public void OnLevelFail(int level)
    {
        #region Facebook
        //LogFacebookEvent("FailLevel", "level", level.ToString());
        #endregion

        #region GameAnalytics
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level_" + level);
        #endregion
    }
    private void InitAdjust(string adjustAppToken)
    {
        var adjustConfig = new AdjustConfig(
            adjustAppToken,
            AdjustEnvironment.Production, // AdjustEnvironment.Sandbox to test in dashboard
            true
        );
        adjustConfig.setLogLevel(AdjustLogLevel.Info); // AdjustLogLevel.Suppress to disable logs
        adjustConfig.setSendInBackground(true);
        new GameObject("Adjust").AddComponent<Adjust>(); // do not remove or rename
        
        Adjust.start(adjustConfig);
    }
}
