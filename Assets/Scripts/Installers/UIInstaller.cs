using UnityEngine;
using Zenject;

public class UIInstaller : MonoInstaller
{
    [SerializeField] private WinPanel _winPanel;
    [SerializeField] private FailPanel _failPanel;
    [SerializeField] private FlashPanel _flashPanel;
    [SerializeField] private PlayerHealthPanel _playerHealthPanel;

    public override void InstallBindings()
    {
        Container.BindInstance(_winPanel).AsSingle();
        Container.BindInstance(_failPanel).AsSingle();
        Container.BindInstance(_flashPanel).AsSingle();
        Container.BindInstance(_playerHealthPanel).AsSingle();
    }
}