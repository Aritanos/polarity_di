using UnityEngine;
using Zenject;

public class MainSceneInstaller : MonoInstaller
{
    [SerializeField] private IntegrationManager _integrationManager;
    
    public override void InstallBindings()
    {
        InstallIntegrations();
    }

    private void InstallIntegrations()
    {
        Container.BindInstance(_integrationManager);
    }
}