using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
    [SerializeField] private PlayerConfig _playerConfig;
    [SerializeField] private EnemyConfig _enemyConfig;
    [SerializeField] private GlobalConfig _globalConfig;
    
    public override void InstallBindings()
    {
        Container.BindInstance(_playerConfig).AsSingle();
        Container.BindInstance(_enemyConfig).AsSingle();
        Container.BindInstance(_globalConfig).AsSingle();
        
        InstallSignals();
        InstallPlayerModules();
        InstallLogic();
        InstallLevelSpawn();
        InstallInput();
    }

    private void InstallLevelSpawn()
    {
        Container.BindFactory<string, UniTask<LevelObject>, LevelObject.Factory>().FromFactory<LevelFactory>();
        Container.Bind<LevelSpawnHandler>().AsSingle();
    }
    
    private void InstallSignals()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<GameStateSignal>();
        Container.DeclareSignal<CheckPointSignal>();
    }
    
    private void InstallPlayerModules()
    {
        Container.Bind<PlayerHealth>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerMovement>().AsSingle();
        Container.BindFactory<string, UniTask<Player>, Player.Factory>().FromFactory<PlayerFactory>();
    }
    
    private void InstallLogic()
    {
        Container.Bind<GameStateHandler>().AsSingle();
        Container.Bind<GravityDirectionHandler>().AsSingle().NonLazy();
    }

    private void InstallInput()
    {
        #if UNITY_EDITOR
        Container.BindInterfacesAndSelfTo<MouseInputHandler>().AsSingle().NonLazy();
#elif UNITY_ANDROID && !UNITY_EDITOR
        //create mobile input handler
#endif
    }
}