using UnityEngine;

public class EnemyVisualsHandler : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private ParticleSystem _charge;
    [SerializeField] private ParticleSystem _fire;

    public void SetAnimatorEnabled(bool isEnabled) => _animator.enabled = isEnabled;
    public void SetAnimationState(bool isActive) => _animator.SetBool("Active", isActive);

    public void ActivateFire()
    {
        _animator.SetTrigger("Attack");
        _fire.Play();
    }
    
    public void StopParticles()
    {
        _fire.Stop();
        _charge.Stop();
    }
    
    public void StartParticles()
    {
        _charge.Play();
    }
}