using UnityEngine;
using Zenject;

public class ThickEnemy : Enemy
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private BoxCollider _boxCollider;

    public override void DisableMoving()
    {
        base.DisableMoving();
        _rigidbody.isKinematic = true;
        _rigidbody.detectCollisions = true;
        _boxCollider.enabled = true;
    }

    public override void EnableRagdoll()
    {
        base.EnableRagdoll();
        _rigidbody.mass = 0;
        _rigidbody.detectCollisions = false;
        _boxCollider.enabled = false;
    }
    protected override void OnSwipe(SwipeDirection swipeDirection) { }
    
    [Inject]
    private void Initialize(EnemyConfig enemyConfig)
    {
        _attackPeriod = enemyConfig.AttackPeriod;
        _damageAmount = enemyConfig.DamageAmount;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Weapon") || other.CompareTag("Enemy"))
        {
            _currentBehaviour = new EnemyDeadBehaviour(this);
            _checkpoint.OnEnemyDefeated(this);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.collider.tag);
        if (collision.collider.CompareTag("Weapon"))
        {
            _currentBehaviour = new EnemyDeadBehaviour(this);
            _checkpoint.OnEnemyDefeated(this);
        }
    }
}
