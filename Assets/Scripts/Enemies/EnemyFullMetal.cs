using UnityEngine;
using Zenject;

public class EnemyFullMetal : Enemy
{
    [SerializeField] private BoxCollider _boxCollider;
    [SerializeField] private Rigidbody _rigidBody;

    public override void DisableRagdoll()
    {
        base.DisableRagdoll();
        _rigidBody.isKinematic = true;
        _rigidBody.detectCollisions = false;
    }

    public override void EnableRagdoll()
    {
        base.EnableRagdoll();
        _boxCollider.isTrigger = true;
        _rigidBody.mass = 0;
        _rigidBody.detectCollisions = false;
    }

    public override void EnableMoving()
    {
        _rigidBody.isKinematic = false;
        _rigidBody.detectCollisions = true;
    }
    
    protected override void OnSwipe(SwipeDirection swipeDirection)
    {
        if (_currentBehaviour is EnemyAttackingBehaviour)
            EnableMoving();
    }
    
    [Inject]
    private void Initialize(EnemyConfig config)
    {
        _attackPeriod = config.AttackPeriod;
        _damageAmount = config.DamageAmount;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Trap"))
        {
            _currentBehaviour = new EnemyDeadBehaviour(this);
            _checkpoint.OnEnemyDefeated(this);
        }
    }
}
