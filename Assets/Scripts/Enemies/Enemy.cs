using System.Collections;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(EnemyVisualsHandler))]
public abstract class Enemy : MonoBehaviour
{
    public EnemyVisualsHandler EnemyVisualsHandler => _enemyVisualsHandler;

    [SerializeField] private EnemyVisualsHandler _enemyVisualsHandler;
    [SerializeField] protected Rigidbody[] _ragdollRigidBody;

    protected Checkpoint _checkpoint;
    protected int _attackPeriod;
    protected int _damageAmount;
    protected EnemyBehaviour _currentBehaviour;
    
    private PlayerHealth _playerHealth;
    private Coroutine _attackCoroutine;
    private IInputHandler _inputHandler;

    public void StopAttacking()
    {
        if (_attackCoroutine!=null)
            StopCoroutine(_attackCoroutine);
    }

    public void StartAttacking() => _attackCoroutine = StartCoroutine(StartAttackIEnum());
    
    public void SetCheckpoint(Checkpoint checkPoint) => _checkpoint = checkPoint;

    public virtual void EnableMoving() { }

    public virtual void DisableMoving() { }
    
    public virtual void EnableRagdoll() => SetKinematic(false, true);
    public virtual void DisableRagdoll() => SetKinematic(true, false);

    protected abstract void OnSwipe(SwipeDirection swipeDirection);
    
    [Inject]
    private void Inject(PlayerHealth playerHealth, IInputHandler inputHandler)
    {
        _playerHealth = playerHealth;
        _inputHandler = inputHandler;
        inputHandler.OnSwipe += OnSwipe;
    }
    
    private void Start()
    {
        _checkpoint.OnCheckpointActivated += OnCheckpointActivated;
        _currentBehaviour = new EnemyStandingBehaviour(this);
    }
    
    private void OnDestroy()
    {
        _checkpoint.OnCheckpointActivated -= OnCheckpointActivated;
        _inputHandler.OnSwipe -= OnSwipe;
        StopAttacking();
    }
    
    private void SetKinematic(bool isKinematic, bool isColliding)
    {
        foreach (Rigidbody rigidbody in _ragdollRigidBody)
        {
            rigidbody.isKinematic = isKinematic;
            rigidbody.detectCollisions = isColliding;
        }
    }
    
    private void OnCheckpointActivated(int checkPointNumber)
    {
        if (_checkpoint.PointNumber == checkPointNumber)
        {
            _currentBehaviour = new EnemyAttackingBehaviour(this);
        }
    }

    private IEnumerator StartAttackIEnum()
    {
        yield return new WaitForSeconds(_attackPeriod);
        DamagePlayer(_damageAmount);
        _attackCoroutine = StartCoroutine(StartAttackIEnum());
    }

    private void DamagePlayer(int damage)
    {
        _playerHealth.ChangeHealth(-damage);
        _enemyVisualsHandler.ActivateFire();
    }
}