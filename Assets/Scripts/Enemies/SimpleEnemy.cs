using Zenject;

public class SimpleEnemy : Enemy
{
    [Inject]
    private void Initialize(EnemyConfig config)
    {
        _attackPeriod = config.AttackPeriod;
        _damageAmount = config.DamageAmount;
    }

    protected override void OnSwipe(SwipeDirection swipeDirection)
    {
        if (_currentBehaviour is EnemyAttackingBehaviour)
        {
            _currentBehaviour = new EnemyDeadBehaviour(this);
            _checkpoint.OnEnemyDefeated(this);
        }
    }
}
